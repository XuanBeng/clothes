import re

from rest_framework import serializers
from rest_framework_jwt.settings import api_settings

from . import models

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
jwt_decode_handler = api_settings.JWT_DECODE_HANDLER

class UserLoginSerializer(serializers.ModelSerializer):

    # 自定义反序列化字段，查询无关
    account = serializers.CharField(write_only=True)
    password = serializers.CharField(write_only=True)

    class Meta:
        model = models.User
        fields = ['account', 'password']
        extra_kwargs = {

        }

    def validate(self, attrs):
        account = attrs.get('account')
        password = attrs.get('password')

        if re.match(r'.+@.+', account):
            user_query = models.User.objects.filter(email= account)
        elif re.match(r'1[3-9][0-9]{9}', account):
            user_query = models.User.objects.filter(mobile=account)
        else:
            user_query = models.User.objects.filter(username=account)
        user = user_query.first()
        if user:
            if user.check_password(password):
                # 签发token
                payload = jwt_payload_handler(user)
                token = jwt_encode_handler(payload)
                self.token = token
                print(token)
                return user
            else:
                # 后期自定义封装错误信息
                raise serializers.ValidationError({'detail': 'password is wrong'})
        else:
            raise serializers.ValidationError({'detail': 'account is not exist'})


class UserDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.User
        fields = ('id', 'username', 'phone', 'grade', 'is_active')


class ClothesSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Clothes
        # fields = '__all__'
        fields = ('id', 'name', 'size', 'manufacturer_name', 'manufacturer', 'description')
        extra_kwargs = {
            'id': {
                'read_only': True
            },
            'manufacturer_name': {
                'read_only': True
            },
            'manufacturer': {
                'write_only': True
            }

        }

class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Client
        fields = ('id', 'name', 'phone', 'grade', 'username', 'create_date', 'operation_user')
        extra_kwargs = {
            'username': {
                'read_only': True
            },
            'operation_user': {
                'write_only': True
            }
        }

class ManufacturerSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Manufacturer
        fields = '__all__'

class InventorySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Inventory
        fields = ('id', 'clothes' ,'clothes_name', 'manufacturer_name', 'amount', 'sale_amount', 'price_in', 'price_out')
        extra_kwargs = {
            'id': {
                'read_only': True
            },
            'clothes_name': {
                'read_only': True
            },
            'manufacturer_name': {
                'read_only': True
            },
            'clothes': {
                'write_only': True
            },

        }

class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Order
        fields = '__all__'