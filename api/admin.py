from django.contrib import admin

from . import models
# Register your models here.
admin.site.register(models.User)
admin.site.register(models.Clothes)
admin.site.register(models.Client)
admin.site.register(models.Inventory)
admin.site.register(models.Manufacturer)
admin.site.register(models.Order)
