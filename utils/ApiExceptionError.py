from rest_framework import serializers, status

class ApiExceptionError(Exception):
    def __init__(self, arg):
        self.msg = arg

class ApiValidationError(serializers.ValidationError):
    status_code = status.HTTP_200_OK
