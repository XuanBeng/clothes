from django.conf.urls import url
from django.urls import path
from rest_framework_jwt.views import obtain_jwt_token
from . import views

urlpatterns = [
    url(r'^user_detail/$', views.UserDetail.as_view()),
    url(r'^login/$', views.UserLoginView.as_view()),

    url(r'^clothes/$', views.ClothesApiView.as_view()),
    url(r'^clothes/(?P<pk>.*)/$', views.ClothesApiView.as_view()),

    url(r'^client/$', views.ClientApiView.as_view()),
    url(r'^client/(?P<pk>.*)/$', views.ClientApiView.as_view()),

    url(r'^manufacturer/$', views.ManufacturerApiView.as_view()),
    url(r'^manufacturer/(?P<pk>.*)/$', views.ManufacturerApiView.as_view()),

    url(r'^inventory/$', views.InventoryApiView.as_view()),
    url(r'^inventory/(?P<pk>.*)/$', views.InventoryApiView.as_view()),

    url(r'^order/$', views.OrderApiView.as_view()),
    url(r'^order/(?P<pk>.*)/$', views.OrderApiView.as_view()),
]
