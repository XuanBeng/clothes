# Create your views here.
from rest_framework import filters
from rest_framework.views import APIView

from . import serializers, models
from utils.response import ApiResponse
from utils.modelViewSet import ApiModelViewSet
from utils.ApiExceptionError import ApiExceptionError

class UserDetail(ApiModelViewSet):

    # authentication_classes = [JwtAuthentication]
    # permission_classes = [IsAuthenticated]
    queryset = models.User.objects.all()
    serializer_class = serializers.UserDetailSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['id']

    def post(self,  request, *args, **kwargs):
        try:
            username = request.data.get('username')
            password = request.data.get('password')
            phone = request.data.get('phone')
            if username:
                if models.User.objects.filter(username= username):
                    raise ApiExceptionError('用户名已存在')
            else:
                raise ApiExceptionError('用户名为空')

            if phone:
                if models.User.objects.filter(phone= phone):
                    raise ApiExceptionError('手机号已存在')
            else:
                raise ApiExceptionError('手机号为空')

            if password == None:
                raise ApiExceptionError('密码为空')
            models.User.objects.create_user(username=username, password=password, phone=phone)
            return ApiResponse()
        except ApiExceptionError as e:
            print(e.msg)
            return ApiResponse(data_status=1, data_msg=e.msg)

class UserLoginView(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self, request, *args, **kwargs):
        ser = serializers.UserLoginSerializer(data= request.data)
        if ser.is_valid(raise_exception=False):
            return ApiResponse(token=ser.token)
        else:
            return ApiResponse(data_status=1, data_msg=ser.errors)

class ClothesApiView(ApiModelViewSet):
    queryset = models.Clothes.objects.filter(is_delete=False).all()
    serializer_class = serializers.ClothesSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['id']

class ClientApiView(ApiModelViewSet):
    queryset = models.Client.objects.filter(is_delete=False).all()
    serializer_class = serializers.ClientSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['id']

class ManufacturerApiView(ApiModelViewSet):
    queryset = models.Manufacturer.objects.filter(is_delete=False).all()
    serializer_class = serializers.ManufacturerSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['id']

class InventoryApiView(ApiModelViewSet):
    queryset = models.Inventory.objects.filter(is_delete=False).all()
    serializer_class = serializers.InventorySerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['id']

class OrderApiView(ApiModelViewSet):
    queryset = models.Order.objects.filter(is_delete=False).all()
    serializer_class = serializers.OrderSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['id']