from rest_framework.permissions import BasePermission
from rest_framework import exceptions

import jwt
from rest_framework_jwt.authentication import BaseJSONWebTokenAuthentication, jwt_decode_handler, get_authorization_header

class JwtAuthentication(BaseJSONWebTokenAuthentication):
    def authenticate(self, request):
        """
        Returns a two-tuple of `User` and token if a valid signature has been
        supplied using JWT-based authentication.  Otherwise returns `None`.
        """
        token = self.parse_token(request)
        if token is None:
            return None

        try:
            payload = jwt_decode_handler(token)
        except jwt.ExpiredSignature:
            msg = 'Signature has expired.'
            raise exceptions.AuthenticationFailed(msg)
        except jwt.DecodeError:
            msg = 'Error decoding signature.'
            raise exceptions.AuthenticationFailed(msg)
        except jwt.InvalidTokenError:
            raise exceptions.AuthenticationFailed()

        user = self.authenticate_credentials(payload)

        return (user, token)

    # token的加密规则解析方法：可定义前缀后缀
    def parse_token(self, request):
        auth = get_authorization_header(request)
        # 无前缀
        token = auth
        return token

# 自定义权限类
class MyPermission(BasePermission):
    def has_permission(self, request, view):
        return True
