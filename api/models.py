from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.

# 权限六表：用户表、角色表、权限表;
#          用户与角色关系表、用户与权限关系表、角色与权限关系表;

# 用户表：访问角色表字段名groups,访问权限字段名user_permissions;
# 角色表：用户user_set, 权限permissions;
# 权限表：用户user_set, 角色group_set;

class User(AbstractUser):
    phone = models.CharField(max_length=11, unique=True)
    grade = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = "api_user"
        verbose_name = "用户表"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.username

class BaseModel(models.Model):
    # id = models.UUIDField(primary_key=True, auto_created=True, default=uuid.uuid4, editable=False)
    is_delete = models.BooleanField(default=False)
    create_date = models.DateTimeField(auto_now_add=True)
    remark = models.CharField(max_length=255, null=True, blank=True)

    class Meta:
        abstract = True

class Clothes(BaseModel):
    name = models.CharField(max_length=100)
    manufacturer = models.ForeignKey(to='Manufacturer', on_delete=models.DO_NOTHING, related_name='clothesList')
    size = models.CharField(max_length=10)
    description = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    @property
    def manufacturer_name(self):
        return self.manufacturer.name

    class Meta:
        db_table = "c_clothes"
        verbose_name = "衣服表"
        verbose_name_plural = verbose_name


class Manufacturer(BaseModel):
    name = models.CharField(max_length=100)
    phone = models.CharField(max_length=15)
    address = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "c_manufacturer"
        verbose_name = "厂商表"
        verbose_name_plural = verbose_name

class Client(BaseModel):
    name = models.CharField(max_length=100)
    phone = models.CharField(max_length=15)
    grade = models.IntegerField(null=True, blank=True)
    operation_user = models.ForeignKey(to='User', on_delete=models.DO_NOTHING, related_name='clientList')

    @property
    def username(self):
        return self.operation_user.username
    def __str__(self):
        return self.name

    class Meta:
        db_table = "c_client"
        verbose_name = "客户表"
        verbose_name_plural = verbose_name

class Inventory(BaseModel):
    clothes = models.OneToOneField(to='Clothes', on_delete=models.DO_NOTHING, related_name='inventory')
    amount = models.IntegerField()
    sale_amount = models.IntegerField(null=True, blank=True)
    price_in = models.DecimalField(max_digits=10, decimal_places=2)
    price_out = models.DecimalField(max_digits=10, decimal_places=2)

    @property
    def clothes_name(self):
        return self.clothes.name

    @property
    def manufacturer_name(self):
        return self.clothes.manufacturer_name

    def __str__(self):
        return self.clothes.name
    class Meta:
        db_table = "inventory"
        verbose_name = "库存表"
        verbose_name_plural = verbose_name

class Order(BaseModel):
    clothes = models.ForeignKey(to='Clothes', on_delete=models.DO_NOTHING, related_name='orderList')
    client = models.ForeignKey(to='Client', on_delete=models.DO_NOTHING, related_name='orderList')
    amount = models.IntegerField()
    total_price = models.DecimalField(max_digits=15, decimal_places=2)
    operation_user = models.ForeignKey(to='User', on_delete=models.DO_NOTHING, related_name='orderList')

    def __str__(self):
        return self.clothes.name
    class Meta:
        db_table = "order"
        verbose_name = "订单表"
        verbose_name_plural = verbose_name

class BackOrder(BaseModel):
    order = models.OneToOneField(to='Order', on_delete=models.DO_NOTHING, related_name='backOrder')
    operation_user = models.ForeignKey(to='User', on_delete=models.DO_NOTHING, related_name='backOrderList')

    class Meta:
        db_table = "back_order"
        verbose_name = "退货表"
        verbose_name_plural = verbose_name