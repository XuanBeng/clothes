from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework import mixins
from rest_framework.generics import GenericAPIView

from utils.response import ApiResponse


def resp():
    res = {
        'code': 0,
        'message': None,
        'data': {}
    }
    return res


# class modelViewSet(viewsets.ModelViewSet):
#     def list(self, request, *args, **kwargs):
#         try:
#             res = super().list(request, *args, **kwargs)
#             return Response({'errCode': 0, 'errMessage': '', 'data': res.data})
#         except Exception as e:
#             print(e)
#             return Response({'errCode': 500, 'errMessage': '查询失败', 'data': {}})
#
#     def retrieve(self, request, *args, **kwargs):
#         instance = self.get_object()
#         serializer = self.get_serializer(instance)
#         return Response({'errCode': 0, 'errMessage': '', 'data': serializer.data})
#
#     def update(self, request, *args, **kwargs):
#         print('yzModelViewset', request.data)
#         res = super().update(request, *args, **kwargs)
#         return Response({'errCode': 0, 'errMessage': '', 'data': {}})
#
#     def destroy(self, request, *args, **kwargs):
#         res = super().destroy(request, *args, **kwargs)
#         return Response({'errCode': 0, 'errMessage': '删除成功', 'data': {}})
#
#     def create(self, request, *args, **kwargs):
#         res = super().create(request, *args, **kwargs)
#         return Response({'errCode': 0, 'errMessage': '新增成功', 'data': {}})
#
#
# class FalseDeleteModelViewSet(viewsets.ModelViewSet):
#     def list(self, request, *args, **kwargs):
#         try:
#             res = super().list(request, *args, **kwargs)
#             return Response({'errCode': 0, 'errMessage': '', 'data': res.data})
#         except Exception as e:
#             print(e)
#             return Response({'errCode': 500, 'errMessage': '查询失败', 'data': {}})
#
#     def retrieve(self, request, *args, **kwargs):
#         instance = self.get_object()
#         serializer = self.get_serializer(instance)
#         return Response({'errCode': 0, 'errMessage': '', 'data': serializer.data})
#
#     def update(self, request, *args, **kwargs):
#         res = super().update(request, *args, **kwargs)
#         return Response({'errCode': 0, 'errMessage': '', 'data': {}})
#
#     def destroy(self, request, *args, **kwargs, ):
#         try:
#             self.get_queryset().filter(id=kwargs['pk']).update(if_deleted=True)
#             return Response({'errCode': 0, 'errMessage': '删除成功', 'data': {}})
#         except Exception as e:
#             print(e)
#             return Response({'errCode': 500, 'errMessage': '删除失败', 'data': {}})
#
#
#     def create(self, request, *args, **kwargs):
#         res = super().create(request, *args, **kwargs)
#         return Response({'errCode': 0, 'errMessage': '新增成功', 'data': {}})


class ApiModelViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, mixins.CreateModelMixin, mixins.UpdateModelMixin, GenericAPIView):

    def get(self, request, *args, **kwargs):
        if 'pk' in kwargs:
            result = self.retrieve(request, *args, **kwargs)
        else:
            result = self.list(request, *args, **kwargs)
        return ApiResponse(data_res=result.data)

    def post(self, request, *args, **kwargs):
        request_data = request.data
        if isinstance(request_data, dict):
            many = False
        elif isinstance(request_data, list):
            many = True
        else:
            many = False
            ApiResponse(data_status=1, data_msg='Incorrect data format')

        serializer = self.get_serializer(data=request.data, many = many)

        if serializer.is_valid(raise_exception=False):
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            return ApiResponse(data_res=serializer.data, http_status=status.HTTP_201_CREATED, headers= headers)
        else:
            print(serializer.errors)
            error = dict(serializer.errors)
            return ApiResponse(data_status=1, data_msg=error)

    def put(self, request, *args, **kwargs):

        # partial = kwargs.pop('partial', False)
        request_data = request.data
        if not isinstance(request_data, dict):
            return ApiResponse(data_status=1, data_msg='Incorrect data format')
        partial = True
        if 'pk' not in kwargs:
            return ApiResponse(data_status=1, data_msg='Please give a id in Url')
        instance = self.get_object()
        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        # if not instance:
        #     return ApiResponse(data_status=1, data_msg="The data you are updating for does not exist")

        serializer = self.get_serializer(instance, data=request_data, partial=partial)
        if serializer.is_valid(raise_exception=False):
            self.perform_update(serializer)
            return ApiResponse(data_res=serializer.data)
        else:
            return ApiResponse(data_status=1, data_msg=serializer.errors)

    def delete(self, request, *args, **kwargs):
        try:
            if 'pk' not in kwargs:
                return ApiResponse(data_status=1, data_msg='Please give a id in Url')
            self.get_queryset().filter(id=kwargs['pk']).update(is_delete=True)
            return ApiResponse()
        except Exception as e:
            return ApiResponse(data_status=1, data_msg='The data you are deleting does not exist')


